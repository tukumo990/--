package com.example.memo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Context;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.LinearLayout;
import android.view.View;
import android.view.View.OnClickListener;





public class MainActivity extends Activity implements OnClickListener,OnItemClickListener{
	
	EditText et1,et2,et3,et4;
	Button bt1,bt2;
	String fp1,fp2,title,fname,sP;
	ScrollView SV,newSV;
	LinearLayout LL,newLL;
	ListView LV;
	ArrayAdapter<String> aa;
	File[] files;
	String[] fna;
	
    @Override
    public void onResume(){
    	super.onResume();
    	try{
    	 setContentView(R.layout.activity_main);
    	}catch(Exception e){}
    	
        et1 = (EditText)findViewById(R.id.editText1);
        et2 = (EditText)findViewById(R.id.editText2);        
        et3 = (EditText)findViewById(R.id.editText3);
        et4 = (EditText)findViewById(R.id.editText4);
        bt1 = (Button)findViewById(R.id.button1);
        bt2 = (Button)findViewById(R.id.button2);
        LL = (LinearLayout)findViewById(R.id.LinearLayout);
        LV = (ListView)findViewById(R.id.listView1); 

        newLL = new LinearLayout(this);
        newSV = new ScrollView(this);
        newLL.setOrientation(LinearLayout.VERTICAL);;
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
    	
    	aa = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        
        
    	
    	sP = this.getFilesDir().getPath().toString();
    	files = new File(sP).listFiles();
    	Arrays.sort (files,Collections.reverseOrder());
    	
    	
    	
    	
    	for(int i=0;i<files.length;i++){
    		fname = files[i].getName();
	
    		fna[0] =fname;
    		
    		if(files[i].isFile() && fname.endsWith(".txt")) {
    			
    			try{
    			
    			FileInputStream fis = openFileInput(fname);
            	byte[] readBytes = new byte[fis.available()];
            	fis.read(readBytes);
            	String all = new String(readBytes);
            	String gyouwari[] = all.split("\n");
            	title=gyouwari[0];
    			}catch(Exception e){}
            	
    			aa.add(title);		
            	
    		}
    			
    	}
    	LV.setAdapter(aa);
    	LV.setOnItemClickListener(this);

    }
    	
    	

    
    
    
    

    @Override
    public void onClick(View v){
    	if(v == bt1){
    		try{
    			
    			
    				Date date = new Date(System.currentTimeMillis());
    				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.JAPAN);
    				fp1 = sdf.format(date) + ".txt";
    			
    			
    			FileOutputStream fos = openFileOutput(fp1,Context.MODE_PRIVATE);
    			String code1 = et1.getText().toString();
    			String code2 = et2.getText().toString();
    			String code3 = et3.getText().toString();
    			String code4 = et4.getText().toString();
    			String code5 = code1+"\n"+code2+"\n"+code3+"\n"+code4;
    			
    			fos.write(code5.getBytes());
    			fos.close();
    		}catch(Exception e){};
    		
    	}else if(v == bt2){
    		
    		try{    		
    		et1.setText("");
    		et2.setText("");
    		et3.setText("");
    		et4.setText("");
    		deleteFile(fname);
    		
    		}catch(Exception e){}
    		
    	}
    }
    @Override
    public void onItemClick(AdapterView<?> parent,View v,int position,long id){
    		try{
				
	        	FileInputStream fis = openFileInput(fname);
	        	byte[] readBytes = new byte[fis.available()];
	        	fis.read(readBytes);
	        	String all = new String(readBytes);
	        	String gyouwari[] = all.split("\n");
	        	et1.setText(gyouwari[0]);
	        	et2.setText(gyouwari[1]);
	        	et3.setText(gyouwari[2]);
	        	et4.setText(gyouwari[3]);
				
	        	fis.close();
	        	
    		}catch(Exception e){}	
    }
    		
}

	


